DROP TRIGGER IF EXISTS account_deposit;
DROP TRIGGER IF EXISTS account_withdraw;
DELIMITER $$

CREATE TRIGGER account_deposit
	AFTER INSERT ON Deposit
	FOR EACH ROW
BEGIN
	IF(NEW.Amount > 0) THEN
		UPDATE Account SET
		Balance = Balance + NEW.Amount
		WHERE ACC_No = NEW.ACC_No;
	END IF;
END $$

CREATE TRIGGER account_withdraw
	AFTER INSERT ON Withdraw
        FOR EACH ROW
BEGIN
        IF(NEW.Amount > 0) THEN
                UPDATE Account SET
               Balance = Balance + NEW.Amount
                WHERE ACC_No = NEW.ACC_No;
        END IF;
END $$
DELIMITER ;

