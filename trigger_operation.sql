DROP TRIGGER IF EXISTS account_operation;
DELIMITER $$
CREATE TRIGGER account_operation
        AFTER INSERT On Operation
        FOR EACH ROW
BEGIN
        IF(New.Action = 'D')THEN
                IF(NEW.Amount > 0) THEN
                        UPDATE Account SET
                        Balance = Balance + NEW.Amount
                        WHERE ACC_No = NEW.ACC_No_Source;
                END IF;
        ELSEIF (New.Action = 'W') THEN
                IF (NEW.Amount > 0) THEN
                        UPDATE Account SET
                        Balance = Balance - New.Amount
                        WHERE ACC_No = NEW.ACC_No_Source;
                END IF;
        ELSEIF (New.Action = 'T') THEN
                IF(NEW.Amount > 0) THEN
                        UPDATE Account SET
                        Balance = Balance - New.Amount
                        WHERE ACC_No = NEW.ACC_No_Source;

                        UPDATE Account SET
                        Balance = Balance + New.Amount
                        WHERE ACC_No = NEW.ACC_No_Dest;

                END IF;

        END IF;
END $$
DELIMITER ;
