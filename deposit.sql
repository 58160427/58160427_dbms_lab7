CREATE TABLE Deposit
(
		Tran_No		INT NOT NULL AUTO_Increment,
		ACC_No		VARCHAR(10) NOT NULL,
		DateOp		DATETIME NOT NULL,
		Amount		FLOAT NOT NULL,
		PRIMARY KEY(Tran_No),
		FOREIGN KEY(ACC_No) REFERENCES Account(ACC_No)
) Engine=InnoDB;

CREATE TABLE Withdraw
(
		Tran_No         INT NOT NULL AUTO_Increment,
                ACC_No          VARCHAR(10) NOT NULL,
                DateOp          DATETIME NOT NULL,
                Amount          FLOAT NOT NULL,
		PRIMARY KEY(Tran_No),
                FOREIGN KEY(ACC_No) REFERENCES Account(ACC_No)
) Engine=InnoDB;

	
